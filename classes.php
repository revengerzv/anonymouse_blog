<?php
/**
 * Created by PhpStorm.
 * User: revenger
 * Date: 28.11.16
 * Time: 11:45
 */

class db {

    private $dbname = 'vjetblog';
    private $dbpass = '';
    private $dbuser = 'root';
    private $dbhost = '127.0.0.1';
    public $dbconn;


    /**
     * @return PDO
     * Connect to DB
     */
    public function connect()
    {
        if (null == $this->dbconn)
        {
            try
            {
                $this->dbconn =  new PDO( "mysql:host=".$this->dbhost.";"."dbname=".$this->dbname.'', $this->dbuser, $this->dbpass);
                $this->dbconn->exec("set names utf8");
            }
            catch(PDOException $e)
            {
                die($e->getMessage());
            }
        }
        return $this->dbconn;
    }

    /**
     * @param $title
     * @param $short_description
     * @param $full_description
     * @param $author
     * @return string
     * Add new post
     */
    public function addPost($title, $short_description, $full_description, $author)
    {
        $q = ("INSERT INTO `posts` ( `name`, `short_description`, `full_description`, `author` ) VALUES ('".$title."', '".$short_description."', '".$full_description."', '".$author."')");
        $this->connect()->query($q);
        return $this->connect()->lastInsertId();
    }

    /**
     * @param $pid
     * @return mixed
     * Show one post
     */
    public function getPost($pid)
    {
        if($this->dbconn !== false AND $pid)
        {
            $res = $this->connect()->query('SELECT * FROM `posts` WHERE `pid` = '.$pid);
            return $res->fetch();
        }
    }

    /**
     * @param bool $count
     * @param bool $offset
     * @return PDOStatement
     * Show all posts
     */
    public function getPostList($count = false, $offset = false)
    {
        if($count == false)
            $count = 10;
        if($offset == false)
            $offset = 0;

        if($this->dbconn !== false)
        {
            return $this->connect()->query('SELECT * FROM `posts` ORDER BY `published_date` ASC LIMIT '.$offset.', '.$count.'');
        }
    }

}