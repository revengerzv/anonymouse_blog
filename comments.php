<?php
/**
 * Created by PhpStorm.
 * User: revenger
 * Date: 28.11.16
 * Time: 18:44
 */

class comments extends db {

    /**
     * @param $author
     * @param $comment
     * @param $pid
     * @return string
     * Add comment
     */
    public function addComment($author, $comment, $pid)
    {
        $q = ("INSERT INTO `comments` ( `user`, `comment`, `pid` ) VALUES ('".$author."', '".$comment."', '".$pid."')");
        $this->connect()->query($q);
        return $this->connect()->lastInsertId();
    }

    /**
     * @return PDOStatement
     * Shows comments list
     */
    public function getCommentsList($pid)
    {
        return $this->connect()->query('SELECT * FROM `comments` WHERE `pid` = '.$pid.' ORDER BY `date` ASC');
    }

    /**
     * @return PDOStatement
     * Return comments quantity by posts sorted by max quantity
     */
    public function getCommentsCount()
    {
        return $this->connect()->query('SELECT `pid`, COUNT(*) FROM `comments` GROUP BY `pid` ORDER BY COUNT(*) DESC LIMIT 0, 5');
    }
}