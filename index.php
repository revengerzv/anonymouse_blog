<?php
/**
 * Created by PhpStorm.
 * User: revenger
 * Date: 28.11.16
 * Time: 11:36
 */
error_reporting(E_ALL);
require_once ('template/header.html');
require_once ('classes.php');
require_once ('comments.php');
require_once ('tools.php');

// connect
$db = new db();
$db->connect();
require_once ('template/addPost.html');
?>
    <div class="container">
        <div class="row">
            <section class="content">
                                <h1>Список последних статей</h1>
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-container">
                                <table class="table table-filter">
                                    <tbody>

                                    <?php
                                    foreach($db->getPostList() as $row):?>

                                        <tr>
                                            <td>
                                                <div class="media">
                                                    <a href="detail.php?id=<?php echo $row["pid"]?>" class="pull-left">
                                                        <h4 class="title">
                                                            <?php echo $row['name']?>
                                                        </h4>
                                                    </a>
                                                    <div class="media-body">
                                                        <span class="media-meta pull-right"><?php echo $row['published_date']?></span>

                                                        <p class="summary"><?php echo cut($row['short_description'], 100)?></p>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
<?php

if(!empty($_POST)
&& !empty($_POST['title'])
&& !empty($_POST['short_text'])
&& !empty($_POST['full_text'])) {
    $db->addPost($_POST['title'], $_POST['short_text'], $_POST['full_text'], $_POST['author']);
}

?>

    <div class="container">
        <div class="row">
            <h2>Самые комментируемые статьи</h2>
            <?php
            /* slider with most commented posts */
            $most_commented = new comments();
            foreach($most_commented->getCommentsCount() as $row):?>
                <p><a href="detail.php?id=<?php echo $db->getPost($row['pid'])['pid']?>"><?php echo $db->getPost($row['pid'])['name']."(".$row['COUNT(*)'].")"?></a></p>

            <?php endforeach?>

        </div>
    </div>








<?php
require_once ('template/footer.html');