<?php
/**
 * Created by PhpStorm.
 * User: revenger
 * Date: 28.11.16
 * Time: 15:24
 */

require_once ('template/header.html');
require_once ('classes.php');
require_once ('tools.php');
require_once ('comments.php');

// connect
$db = new db();
$db->connect();

// переменной $pid присваиваем значение приведенное к int
// https://habrahabr.ru/post/143035/
$pid = intval($_GET['id']);
if($pid):?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo $db->getPost($pid)['name']?></h1>
                <span class="badge">
                    <?php echo $db->getPost($pid)['published_date']?>
                </span>
                <p>
                    <?php echo $db->getPost($pid)['short_description']?>
                    <?php echo $db->getPost($pid)['full_description']?>
                </p>
            <div
        </div>
    </div>
<?php endif;?>


<?php
/*
    ADD NEW COMMENT
*/
if(!empty($_POST['author'])
&& !empty($_POST['comment'])
&& !empty($_GET['id'])) {

    $comment = new comments();
    $comment->addComment($_POST['author'], $_POST['comment'], $_GET['id']);
    header("Location: /");
    exit();
}

/*
    COMMENTS LIST
 */
require_once ('template/addComment.html');
$comments_list = new comments();
?>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="page-header">
                    <h1>Comments </h1>
                </div>
                <div class="comments-list">
                    <?php
                       foreach($comments_list->getCommentsList($_GET['id']) as $row):?>

                           <div class="media">
                               <p class="pull-right"><small><?php echo $row['date']?></small></p>
                               <div class="media-body">

                                   <h4 class="media-heading user_name"><?php echo $row['user']?></h4>
                                   <?php echo $row['comment']?>
                               </div>
                           </div>
                    <?php endforeach?>
                </div>
            </div>
        </div>
    </div>
<?php
require_once ('template/footer.html');