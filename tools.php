<?php
/**
 * Created by PhpStorm.
 * User: revenger
 * Date: 28.11.16
 * Time: 13:47
 */

/**
 * @param $str
 * @param $length
 * @param string $postfix
 * @param string $encoding
 * @return string
 * function returns string cutted to 100 symbols
 */
function cut($str, $length, $postfix='...', $encoding='UTF-8')
{
    if (mb_strlen($str, $encoding) <= $length) {
        return $str;
    }

    $tmp = mb_substr($str, 0, $length, $encoding);
    return mb_substr($tmp, 0, mb_strripos($tmp, ' ', 0, $encoding), $encoding) . $postfix;
}